# Weighted AUReC

This repository contains the code for the paper "Weighted AUReC: Handling Skew in Shard Map
Quality Estimation for Selective Search".

## Installation

We use [Poetry](https://python-poetry.org/) for dependency management. First, install the dependencies.

    poetry install

From here on, either activate your virtual environment with `poetry shell`, or prefix all your subsequent commands with `poetry run`.

## Usage

First, download run files and qrels from the TREC website. Concatenate the qrel files for each of the datasets, so we have a single qrel file per dataset.

    cat /data/TREC/terabyte/200{4,5,6}/qrels.txt > data/qrels-gov2.txt
    cat /data/TREC/web/20{09,10,11,12}/qrels.txt > data/qrels-cw09b.txt

Then, run the `fusion.py` script, which uses SlideFusion-MAP to fuse the top 10 runs submitted to each year of TREC together.

    for year in 2004 2005 2006 ; do
        python3 fusion.py \
          -o /data/TREC/terabyte/$year/fused.gz \
          -q /data/TREC/terabyte/$year/qrels.txt \
          /data/TREC/terabyte/$year/inputs/*
    done
    
    for year in 2009 2010 2011 2012 ; do
        python3 fusion.py \
          -o /data/TREC/web/$year/fused.gz \
          -q /data/TREC/web/$year/qrels.txt \
          /data/TREC/web/$year/inputs/*
    done
    
    cat /data/TREC/terabyte/*/fused.gz data/runs-gov2.txt.gz
    cat /data/TREC/web/*/fused.gz data/runs-cw09b.txt.gz

Download [the shard maps from Dai et al.](https://boston.lti.cs.cmu.edu/appendices/CIKM2016-Dai/) and place them in a `dai-et-al-shardmaps` directory. Run the `transform_shardmaps.py` script to parse the shardmaps into our `ShardMap` format. This format separates the documents that are always non-relevant from those that are relevant, in order to speed up subsequent experiments.

    python3 transform_shardmaps.py

We can now run our experiment on the original Dai et al. shard maps. This generates a file `results_original.csv`.

    python3 run_experiment.py shardmaps/*

For our experiments with randomized shard maps, we have to randomize the underlying shard maps.

    python3 randomize_shardmaps.py 

This can be used to run the randomized experiment, as well.

    python3 run_experiment.py --random-iterations=50 random-shardmaps/*

This generates a file `results_random.csv`. With both files in place, we can generate the table and plots for in the paper.

    python3 rbr_table.py
    python3 bbr_plots.py
