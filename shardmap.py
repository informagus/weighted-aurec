import json
import random
from collections import Counter, defaultdict
from dataclasses import dataclass
from functools import cached_property
from pathlib import Path
from typing import Optional

from ir_measures import ScoredDoc, Qrel


def distribute_documents(num_docs: int, num_shards: int, distribution_type: str):
    if distribution_type == 'uniform':
        num_per_shard = [num_docs // num_shards for _ in range(num_shards)]
    elif distribution_type == 'linear':
        factor = num_docs / (num_shards * (num_shards + 1) / 2)
        num_per_shard = [max(1, round((i + 1) * factor)) for i in range(num_shards)]
    else:
        factor = num_docs / (num_shards * (num_shards + 1) * (2 * num_shards + 1) / 6)
        num_per_shard = [max(1, round((i + 1) ** 2 * factor)) for i in range(num_shards)]

    # Fix rounding errors
    if sum(num_per_shard) > num_docs:
        for i in range(sum(num_per_shard) - num_docs):
            num_per_shard[-i - 1] -= 1
    elif sum(num_per_shard) < num_docs:
        for i in range(num_docs - sum(num_per_shard)):
            num_per_shard[-i - 1] += 1

    return num_per_shard


@dataclass
class BaseMap:
    shard_sizes: dict[str, int]
    retrieved_docs: dict[str, dict[str, list[ScoredDoc]]]

    @staticmethod
    def load(filename):
        with open(filename) as _file:
            return BaseMap.from_dict(json.load(_file))

    def write(self, filename):
        with open(filename, 'w') as _file:
            json.dump(self.to_dict(), _file)

    def to_dict(self):
        return {
            'shard_sizes': self.shard_sizes,
            'retrieved_docs': self.retrieved_docs,
        }

    @staticmethod
    def from_dict(data):
        retrieved_docs = data['retrieved_docs']

        for query in retrieved_docs:
            for shard in retrieved_docs[query]:
                retrieved_docs[query][shard] = [ScoredDoc(*values) for values in retrieved_docs[query][shard]]

        return BaseMap(data['shard_sizes'], retrieved_docs)

    def shard_size(self, shard: str):
        return self.shard_sizes.get(shard, 0)

    def get_retrieved(self, query: str, shard: str):
        return self.retrieved_docs.get(query, {}).get(shard, [])

    def randomize(self, distribution_type: str) -> 'BaseMap':
        num_shards = random.randint(100, 200)
        num_docs = sum(self.shard_sizes.values())

        retrieved_docs = {
            query: [doc for shard_docs in query_docs.values() for doc in shard_docs]
            for query, query_docs in self.retrieved_docs.items()
        }

        num_per_shard = distribute_documents(num_docs, num_shards, distribution_type)

        shard_sizes = {str(i + 1): num for i, num in enumerate(num_per_shard)}

        new_retrieved_docs = defaultdict(lambda: defaultdict(list))

        for query, docs in retrieved_docs.items():
            random.shuffle(docs)

            assignment = random.choices(list(shard_sizes), num_per_shard, k=len(docs))

            for shard, doc in zip(assignment, docs):
                new_retrieved_docs[query][shard].append(doc)

        return BaseMap(shard_sizes, new_retrieved_docs)


@dataclass
class RelevantMap:
    doc_shard_mapping: dict[str, str]
    retrieved_docs: dict[str, dict[str, list[ScoredDoc]]]
    qrels: dict[str, dict[str, list[Qrel]]]

    @staticmethod
    def load(filename):
        with open(filename) as _file:
            return RelevantMap.from_dict(json.load(_file))

    def write(self, filename):
        with open(filename, 'w') as _file:
            json.dump(self.to_dict(), _file)

    def to_dict(self):
        return {
            'doc_shard_mapping': self.doc_shard_mapping,
            'retrieved_docs': self.retrieved_docs,
            'qrels': self.qrels,
        }

    @staticmethod
    def from_dict(data):
        retrieved_docs = data['retrieved_docs']

        for query in retrieved_docs:
            for shard in retrieved_docs[query]:
                retrieved_docs[query][shard] = [ScoredDoc(*values) for values in retrieved_docs[query][shard]]

        qrels = data['qrels']

        for query in qrels:
            for shard in qrels[query]:
                qrels[query][shard] = [Qrel(*values) for values in qrels[query][shard]]

        return RelevantMap(data['doc_shard_mapping'], retrieved_docs, qrels)

    def get_num_relevant(self, query: str, shard: Optional[str] = None):
        qrels = self.qrels.get(query, {})

        if shard is None:
            return sum(map(len, qrels.values()))

        return len(qrels.get(shard, []))

    @cached_property
    def shard_to_doc_mapping(self):
        mapping = defaultdict(list)

        for doc, shard in self.doc_shard_mapping.items():
            mapping[shard].append(doc)

        return dict(mapping)

    @property
    def shard_sizes(self):
        return {shard: len(docs) for shard, docs in self.shard_to_doc_mapping.items()}

    def shard_size(self, shard: str):
        return self.shard_sizes.get(shard, 0)

    def get_retrieved(self, query: str, shard: str):
        return self.retrieved_docs.get(query, {}).get(shard, [])

    def randomize(self, shard_sizes: dict[str, int]) -> 'RelevantMap':
        qrels_per_query = {
            query: [qrel for qrels in query_relevances.values() for qrel in qrels]
            for query, query_relevances in self.qrels.items()
        }

        shards = sorted(shard_sizes.keys(), key=shard_sizes.get)

        doc_shard_mapping = {}
        mapped_qrels = {}

        for query, qrels in qrels_per_query.items():
            mapping = defaultdict(list)
            remaining_qrels = []

            for qrel in qrels:
                if qrel.doc_id in doc_shard_mapping:
                    mapping[doc_shard_mapping[qrel.doc_id]].append(qrel)
                else:
                    remaining_qrels.append(qrel)

            num_qrels = len(remaining_qrels)

            if num_qrels > 0:
                shard_types = random.choice(['small', 'large', 'random', 'ends'])
                num_shards = random.randint(1, min(10, num_qrels, len(shards)))
                sort_order = random.choice(['asc', 'desc', 'random'])
                distribution_type = random.choice(['uniform', 'linear', 'quadratic'])

                if shard_types == 'small':
                    shard_options = shards[:len(shards) // 4]
                elif shard_types == 'large':
                    shard_options = shards[len(shards) // 4 * 3:]
                elif shard_types == 'ends':
                    shard_options = shards[:len(shards) // 4] + shards[len(shards) // 4 * 3:]
                else:
                    shard_options = shards

                selected_shards = random.sample(shard_options, num_shards)

                if sort_order == 'asc':
                    selected_shards.sort(key=shard_sizes.get)
                elif sort_order == 'desc':
                    selected_shards.sort(key=shard_sizes.get, reverse=True)
                else:
                    random.shuffle(selected_shards)

                num_per_shard = distribute_documents(num_qrels, num_shards, distribution_type)

                # All documents should be distributed
                assert sum(num_per_shard) == num_qrels

                random.shuffle(remaining_qrels)

                for shard, num in zip(selected_shards, num_per_shard):
                    for assigned_qrel in remaining_qrels[:num]:
                        mapping[shard].append(assigned_qrel)
                        doc_shard_mapping[assigned_qrel.doc_id] = shard

                    remaining_qrels = remaining_qrels[num:]

                assert sum(map(len, mapping.values())) == len(qrels)

            if mapping:
                mapped_qrels[query] = dict(mapping)

        retrieved_docs = defaultdict(lambda: defaultdict(list))
        for query, docs_per_shard in self.retrieved_docs.items():
            for docs in docs_per_shard.values():
                for doc in docs:
                    assigned_shard = doc_shard_mapping[doc.doc_id]
                    retrieved_docs[query][assigned_shard].append(doc)

        new_map = RelevantMap(doc_shard_mapping, dict(retrieved_docs), mapped_qrels)

        for query in self.retrieved_docs:
            assert sum(map(len, retrieved_docs.get(query, {}).values())) == sum(map(len, self.retrieved_docs.get(query, {}).values()))

        assert sum(new_map.shard_sizes.values()) == sum(self.shard_sizes.values())

        return new_map


class ShardMap:
    def __init__(self, base_map: BaseMap, relevant_map: RelevantMap):
        self.base_map = base_map
        self.relevant_map = relevant_map

        self.shards = sorted(set(self.base_map.shard_sizes.keys()) | set(self.relevant_map.shard_sizes.keys()))
        self.queries = sorted(set(self.base_map.retrieved_docs.keys()) | set(self.relevant_map.retrieved_docs.keys()))

    @staticmethod
    def load(path: str | Path):
        path = Path(path)
        return ShardMap(
            BaseMap.load(path / 'base.json'),
            RelevantMap.load(path / 'relevant.json')
        )

    def write(self, path: str | Path):
        path = Path(path)
        path.mkdir(exist_ok=True, parents=True)

        self.base_map.write(path / 'base.json')
        self.relevant_map.write(path / 'relevant.json')

    def shard_size(self, shard: str):
        return self.base_map.shard_size(shard) + self.relevant_map.shard_size(shard)

    @cached_property
    def relevance_counts(self):
        counts = defaultdict(dict)

        for query in self.queries:
            for shard in self.shards:
                num_relevant = self.relevant_docs(query, shard)
                if num_relevant > 0:
                    counts[query][shard] = num_relevant

        return dict(counts)

    @property
    def shard_sizes(self):
        return {shard: self.shard_size(shard) for shard in self.shards}

    def relevant_docs(self, query: str, shard: str):
        return self.relevant_map.get_num_relevant(query, shard)

    def fraction_relevant_docs(self, query: str, shard: str):
        return self.relevant_docs(query, shard) / self.shard_size(shard)

    def retrieved_docs(self, query: str, shard: str, k: Optional[int] = None):
        docs = (
            self.base_map.get_retrieved(query, shard) +
            self.relevant_map.get_retrieved(query, shard)
        )

        docs.sort(key=lambda doc: doc.score, reverse=True)

        if k is not None:
            return docs[:k]

        return docs

    def rbr_retrieval(self, query: str, k: int):
        shards = self.rbr_shards(query, k)
        return [doc for _, doc in self.merge_shard_results(query, shards)]

    def rbr_shards(self, query: str, k: int):
        return sorted(self.shards, key=lambda shard: self.relevant_docs(query, shard), reverse=True)[:k]

    def bbr_retrieval(self, query: str, b: int):
        shards = self.bbr_shards(query, b)
        return [doc for _, doc in self.merge_shard_results(query, shards)]

    def bbr_shards(self, query: str, b: int):
        shards = sorted(self.shards, key=lambda shard: self.fraction_relevant_docs(query, shard), reverse=True)

        result = []

        for shard in shards:
            if (size := self.shard_size(shard)) <= b:
                result.append(shard)
                b -= size

        return result

    def merge_shard_results(self, query: str, shards: Optional[list[str]] = None):
        if shards is None:
            shards = self.shards

        results = [
            (shard, doc)
            for sub_map in [self.base_map, self.relevant_map]
            for shard in shards
            for doc in sub_map.get_retrieved(query, shard)
        ]

        results.sort(key=lambda pair: pair[1].score, reverse=True)

        return results

    def pseudo_relevance_counts(self, query: str, k: int):
        results = self.merge_shard_results(query)

        counts = Counter(shard for shard, _ in results[:k])

        return {shard: counts.get(shard, 0) for shard in self.shards}
