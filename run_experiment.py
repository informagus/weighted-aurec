import argparse
from concurrent.futures import ProcessPoolExecutor, as_completed
from pathlib import Path

import ir_measures
import pandas as pd
from ir_measures import *
from tqdm import tqdm

from aurec import aurec, weighted_aurec
from shardmap import ShardMap

INCLUDE_AUGMENTED_SHARDS = False

RBR_PARAMETERS = [1, 3, 5]  # range(1, 6)
# BBR_ABSOLUTE_SIZES = [200_000, 400_000, 600_000, 800_000, 1_000_000]
BBR_BUDGETS = [num * scale for scale in [10_000, 100_000, 1_000_000] for num in range(1, 11)]

TARGET_MEASURE = MAP @ 1000

NUM_RANDOM_PER_MAP = 100


def evaluate_single_shard_map(shard_map: ShardMap, qrels: str | list[ir_measures.Qrel], randomize: bool = True):
    qrels = list(ir_measures.read_trec_qrels(qrels))
    queries = {qrel.query_id for qrel in qrels}

    if randomize:
        rel_map = shard_map.relevant_map.randomize(shard_map.base_map.shard_sizes)
        shard_map = ShardMap(shard_map.base_map, rel_map)

    results = []
    for query in tqdm(queries, leave=False, desc='Query', disable=True):
        query_qrels = [qrel for qrel in qrels if qrel.query_id == query]

        if not query_qrels:
            continue

        pseudo_relevance_counts = shard_map.pseudo_relevance_counts(query, k=1000)

        result = {
            'query': query,
            'aurec': aurec(pseudo_relevance_counts),
            'weighted_aurec': weighted_aurec(pseudo_relevance_counts, shard_map.shard_sizes),
        }

        for k in RBR_PARAMETERS:
            rbr_run = shard_map.rbr_retrieval(query, k)
            result[f'rbr_{k}'] = next(ir_measures.iter_calc([TARGET_MEASURE], query_qrels, rbr_run)).value

        for budget in BBR_BUDGETS:
            bbr_run = shard_map.bbr_retrieval(query, b=budget)

            result[f'bbr_{budget}'] = next(ir_measures.iter_calc([TARGET_MEASURE], query_qrels, bbr_run)).value

        results.append(result)

    return results


def evaluate_variations(shard_map_path: str, num_random_iterations: int | None):
    shard_map_name = Path(shard_map_path).name
    dataset_name = shard_map_name.split("-")[0].lower()

    shard_map = ShardMap.load(shard_map_path)

    if num_random_iterations is None:
        results = evaluate_single_shard_map(shard_map, f'data/qrels-{dataset_name}.txt', randomize=False)
        for result in results:
            result['original'] = shard_map_name
            yield result

        return

    with tqdm(total=num_random_iterations, leave=False) as pbar, ProcessPoolExecutor() as executor:
        futures = []
        for _ in range(num_random_iterations):
            future = executor.submit(evaluate_single_shard_map, shard_map, f'data/qrels-{dataset_name}.txt')
            future.add_done_callback(lambda _: pbar.update())

            futures.append(future)

        for i, future in enumerate(as_completed(futures)):
            for result in future.result():
                result['original'] = shard_map_name
                result['iteration'] = f'random-{i + 1}'
                yield result


def evaluate_all(shard_maps, num_iterations):
    for shard_map in tqdm(shard_maps, desc='Shard map'):
        shard_map_name = shard_map.split('/')[-1].split('.')[0]
        tqdm.write(shard_map_name)

        yield from evaluate_variations(shard_map, num_iterations)


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('-r', '--random-iterations', type=int, default=None,
                        help='Number of random relevant document distributions per shard map (if any)')

    parser.add_argument('base_maps', help='Which base shard maps to use', nargs='+')

    args = parser.parse_args()

    df = pd.DataFrame(evaluate_all(args.base_maps, args.random_iterations))

    if args.random_iterations is None:
        df.to_csv('results_original.csv', index=False)
    else:
        df.to_csv('results_random.csv', index=False)


if __name__ == '__main__':
    main()
