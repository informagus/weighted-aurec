import numpy as np
from sklearn.metrics import auc


def aurec(pseudo_relevance_counts: dict[str, int]):
    counts = sorted(pseudo_relevance_counts.values(), reverse=True)

    shard_nums = np.linspace(0, 1, len(counts) + 1)
    recalls = np.cumsum([0] + counts) / sum(counts)

    return auc(shard_nums, recalls)


def weighted_aurec(relevance_counts: dict[str, int], shard_sizes: dict[str, int]):
    total_relevant = sum(relevance_counts.values())
    total_docs = sum(shard_sizes.values())

    shard_order = sorted(shard_sizes.keys(),
                         key=lambda key: relevance_counts.get(key, 0) / shard_sizes[key],
                         reverse=True)

    recalls = [0] + [relevance_counts.get(shard, 0) / total_relevant for shard in shard_order]
    doc_sizes = [0] + [shard_sizes[shard] / total_docs for shard in shard_order]

    recalls = np.cumsum(recalls)
    doc_sizes = np.cumsum(doc_sizes)

    return auc(doc_sizes, recalls)
