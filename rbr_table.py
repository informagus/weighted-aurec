import pandas as pd
from scipy.stats import pearsonr


def compute_correlations(df):
    if 'iteration' in df.columns:
        df = df.groupby(['original', 'iteration'])[avg_cols].mean()
    else:
        df = df.groupby('original')[avg_cols].mean()

    rows = []
    for a in metrics:
        for b in rbr_cols:
            stat = pearsonr(df[a], df[b])
            rows.append({
                'measure': a,
                'baseline': b,
                'corr': stat.statistic,
            })

    res = pd.DataFrame(rows).pivot(index='baseline', columns='measure', values='corr')

    assert res.columns[0] == 'aurec' and res.columns[1] == 'weighted_aurec'

    res.columns = ['$\\mathit{AUReC}$', '$\\mathit{wAUReC}$']

    return res


df_orig = pd.read_csv('results_original.csv')
df_random = pd.read_csv('results_random.csv')

metrics = [col for col in df_orig.columns if 'aurec' in col]
rbr_cols = [col for col in df_orig.columns if 'rbr' in col]
avg_cols = metrics + rbr_cols

res_orig = compute_correlations(df_orig)
res_random = compute_correlations(df_random)

res_orig.columns = pd.MultiIndex.from_product([('Dai et al.',), res_orig.columns])
res_random.columns = pd.MultiIndex.from_product([('Random',), res_random.columns])

result = pd.concat([res_orig, res_random], axis='columns')

print('Original AUReC/wAUReC correlation:', pearsonr(df_orig['aurec'], df_orig['weighted_aurec']))
print('Random AUReC/wAUReC correlation:', pearsonr(df_random['aurec'], df_random['weighted_aurec']))

result.index = ['RBR ($k = 1$)', 'RBR ($k = 3$)', 'RBR ($k = 5$)']
result.index.name = 'EtE'

style = result.style.format(precision=3).highlight_max(props='bfseries: ;', axis='columns')

caption = """
Correlation (Pearson's $r$) between AUReC variants and end-to-end system using 
relevance-based ranking as resource selection algorithm.
""".strip()

latex = style.to_latex(siunitx=True, position_float='centering', hrules=True,
                       label='tab:rbr_corr', caption=caption,
                       multicol_align='r')

print(latex)
