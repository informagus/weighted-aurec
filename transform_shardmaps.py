import gzip
from collections import defaultdict
from pathlib import Path

import ir_measures
from tqdm import tqdm

from shardmap import BaseMap, RelevantMap, ShardMap


def read_shard_map(shard_map_dir: Path, verbose: bool = True):
    shard_map = {}

    for file in tqdm(list(shard_map_dir.iterdir()), disable=not verbose):
        partition = file.name

        fn_open = gzip.open if partition.endswith('.gz') else open

        with fn_open(file, 'rt') as _file:
            for line in _file:
                shard_map[line.strip()] = partition

    return shard_map


def main():
    old_dir = Path('dai-et-al-shardmaps')
    new_dir = Path('shardmaps')

    for file in old_dir.iterdir():
        if file.name.count('-') > 2:
            continue

        shard_map = read_shard_map(file)

        slug = file.name.split("-")[0].lower()
        qrels = list(ir_measures.read_trec_qrels(f'data/qrels-{slug}.txt'))
        run = list(ir_measures.read_trec_run(f'data/runs-{slug}.txt.gz'))

        base_shard_sizes = defaultdict(int)
        base_retrieved_docs = defaultdict(lambda: defaultdict(list))

        rel_doc_shard_mapping = {}
        rel_retrieved_docs = defaultdict(lambda: defaultdict(list))
        rel_qrels = defaultdict(lambda: defaultdict(list))

        relevant_ids = {qrel.doc_id for qrel in qrels if qrel.relevance > 0}

        for doc_id, shard in tqdm(shard_map.items(), desc='shard count'):
            if doc_id in relevant_ids:
                rel_doc_shard_mapping[doc_id] = shard
            else:
                base_shard_sizes[shard] += 1

        for qrel in tqdm(qrels, desc='qrels'):
            if qrel.relevance > 0 and qrel.doc_id in shard_map:
                shard = shard_map[qrel.doc_id]
                rel_qrels[qrel.query_id][shard].append(qrel)

        for doc in tqdm(run, desc='run'):
            if doc.doc_id in shard_map:
                shard = shard_map[doc.doc_id]

                if doc.doc_id in relevant_ids:
                    rel_retrieved_docs[doc.query_id][shard].append(doc)
                else:
                    base_retrieved_docs[doc.query_id][shard].append(doc)

        base_map = BaseMap(base_shard_sizes, base_retrieved_docs)
        rel_map = RelevantMap(rel_doc_shard_mapping, rel_retrieved_docs, rel_qrels)

        shard_map = ShardMap(base_map, rel_map)
        shard_map.write(new_dir / file.name)


if __name__ == '__main__':
    main()
