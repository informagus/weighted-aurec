from pathlib import Path

from tqdm import tqdm, trange

from shardmap import ShardMap

in_dir = Path('shardmaps')
out_dir = Path('random-shardmaps')

# Since we're generating fully random shard maps, we only need one
# of the original shard maps (per corpus) as starting point
files = ['CW09B-KLD-Rand', 'GOV2-KLD-Rand']

for file in tqdm(files, desc='total'):
    shard_map = ShardMap.load(in_dir / file)

    base_map = shard_map.base_map
    rel_map = shard_map.relevant_map

    for method in tqdm(['uniform', 'linear', 'quadratic'], leave=False, desc=file):
        for i in trange(1, 2, leave=False, desc=method):
            new_map = base_map.randomize(method)
            ShardMap(new_map, rel_map).write(out_dir / f'{file}-{method}-{i}')
