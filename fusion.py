import argparse
import gzip
from collections import defaultdict

import ir_measures
from ir_measures import Qrel, Metric, MAP, ScoredDoc
from tqdm import tqdm


class SlideFuse:
    def __init__(self, measure: Metric = MAP@1000, window_size: int = 5):
        self.measure = measure
        self.window_size = window_size

        self.rank_probabilities = defaultdict(dict)
        self.list_weights = defaultdict(dict)

    def train(self, runs: dict[str, list[ScoredDoc]], qrels: list[Qrel]):
        qrel_mapping = self.qrels_to_mapping(qrels)

        for query_id in tqdm(qrel_mapping, desc='training', leave=False, unit='q'):
            filtered_qrels = [qrel for qrel in qrels if qrel.query_id != query_id]

            for name, run in tqdm(runs.items(), leave=False, unit='r'):
                filtered_run = [doc for doc in run if doc.query_id != query_id]

                num_rel = defaultdict(int)
                num_total = defaultdict(int)

                current_query = None
                rank = 1
                for doc in filtered_run:
                    if doc.query_id != current_query:
                        current_query = doc.query_id
                        rank = 1

                    num_total[rank] += 1

                    if qrel_mapping.get(current_query, {}).get(doc.doc_id, 0) > 0:
                        num_rel[rank] += 1

                    rank += 1

                self.rank_probabilities[query_id][name] = {
                    rank: rel / num_total[rank] for rank, rel in num_rel.items()
                }

                self.list_weights[query_id][name] = ir_measures.calc_aggregate([self.measure], filtered_qrels, filtered_run)[self.measure]

    def fuse(self, runs: dict[str, list[ScoredDoc]]) -> list[ScoredDoc]:
        run_mapping = self.runs_to_mapping(runs)

        result = []

        for query_id, runs in tqdm(run_mapping.items(), desc='fusing', leave=False, unit='q'):
            doc_scores = defaultdict(float)

            for run_name, run in runs.items():
                for rank, doc in enumerate(run):
                    rank = rank + 1

                    a = max(1, rank - self.window_size)
                    b = min(len(run), rank + self.window_size)

                    rank_probs = self.rank_probabilities[query_id].get(run_name, {})
                    slide_score = sum(rank_probs.get(i, 0) for i in range(a, b + 1)) / (b - a + 1)

                    weighted_score = slide_score * self.list_weights[query_id].get(run_name, 0)

                    if weighted_score > 0:
                        doc_scores[doc.doc_id] += weighted_score

            sorted_docs = sorted(doc_scores.items(), key=lambda pair: pair[1], reverse=True)
            for doc_id, score in sorted_docs:
                result.append(ScoredDoc(query_id, doc_id, score))

        return result

    @staticmethod
    def qrels_to_mapping(qrels: list[Qrel]):
        mapping = defaultdict(lambda: defaultdict(float))

        for qrel in qrels:
            mapping[qrel.query_id][qrel.doc_id] = qrel.relevance

        return {query: dict(sub_mapping) for query, sub_mapping in mapping.items()}

    @staticmethod
    def runs_to_mapping(runs: dict[str, list[ScoredDoc]]):
        mapping = defaultdict(lambda: defaultdict(list))

        for name, run in runs.items():
            for doc in run:
                mapping[doc.query_id][name].append(doc)
                mapping[doc.query_id][name].sort(key=lambda d: d.score, reverse=True)

        return {
            query: {
                run_name: sorted(run_results, key=lambda d: d.score, reverse=True)
                for run_name, run_results in runs_per_query.items()
            }
            for query, runs_per_query in mapping.items()
        }


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output', help='Where to write the fused run', required=True)
    parser.add_argument('-q', '--qrels', help='Qrels for the associated task', required=True)
    parser.add_argument('runs', help='Run files to fuse', nargs='+')

    args = parser.parse_args()

    qrels = list(ir_measures.read_trec_qrels(args.qrels))
    runs = {
        file.split('/')[-1].split('.')[-2]: list(ir_measures.read_trec_run(file))
        for file in tqdm(args.runs, desc='reading', leave=False)
    }

    scores = []
    for name, run in tqdm(runs.items(), desc='scoring', leave=False):
        score = ir_measures.calc_aggregate([MAP@1000], qrels, run)[MAP@1000]
        scores.append((name, score))

    scores.sort(key=lambda x: x[1], reverse=True)

    fuse_runs = {name: runs[name] for name, _ in scores[:10]}

    fuser = SlideFuse()

    fuser.train(fuse_runs, qrels)
    fused_run = fuser.fuse(fuse_runs)

    fused_score = ir_measures.calc_aggregate([MAP@1000], qrels, fused_run)[MAP@1000]

    for name, score in scores:
        print(name, score)

    print('\nfused', fused_score)

    open_fn = gzip.open if args.output.endswith('.gz') else open

    with open_fn(args.output, 'wt') as _file:
        current_query = None
        rank = 1
        for doc in fused_run:
            if doc.query_id != current_query:
                current_query = doc.query_id
                rank = 1

            _file.write(f'{doc.query_id} Q0 {doc.doc_id} {rank} {doc.score} slidefusion-map\n')

            rank += 1


if __name__ == '__main__':
    main()
