import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from scipy.stats import pearsonr

df = pd.read_csv('results_random.csv')
df['dataset'] = [x.split('-')[0] for x in df['original']]

cols = [col for col in df.columns if 'bbr' in col]
measures = ['aurec', 'weighted_aurec']

df = df.groupby(['dataset', 'original', 'iteration'])[cols + measures].mean().reset_index()

dfs = {}
for dataset, sub_df in df.groupby('dataset'):
    rows = []

    for col in cols:
        for measure in measures:
            corr = pearsonr(sub_df[measure], sub_df[col])
            row = {
                'Budget': int(col.split('_')[-1]),
                'Correlation': corr.statistic,
                'Measure': {'aurec': 'AUReC', 'weighted_aurec': 'Weighted AUReC'}[measure],
            }

            rows.append(row)

    dfs[dataset] = pd.DataFrame(rows)


min_y = 1.1 * min(df['Correlation'].min() for df in dfs.values())
max_y = 1.1 * max(df['Correlation'].max() for df in dfs.values())


for dataset, df in dfs.items():
    plt.clf()

    ax = sns.lineplot(df, x='Budget', y='Correlation', hue='Measure')
    ax.set_xticks([i * 1_000_000 for i in range(11)],
                  ['0' if i == 0 else f'{i}M' for i in range(11)])
    ax.set(xlabel='Budget (number of documents)', ylabel='Correlation (Pearson\'s $r$)')
    ax.set_ylim((min_y, max_y))
    ax.set_title(dataset)

    # plt.savefig(f'bbr_corr_{dataset.lower()}.pdf')
    plt.show()
